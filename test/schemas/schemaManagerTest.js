"use strict";

var path = require('path')
var SchemaManager = require('../../src/schemas/SchemaManager');
var expect = require('chai').expect;
var pathMockSchema = path.join(__dirname, 'resources/user.json'); 

describe('User tests', () => {
  describe('#getSchema', () => {
    it('Should return an user schema object', (done) => {

      let manager = new SchemaManager();      
      manager.getSchema(pathMockSchema).then((obj) => {
          expect(obj.properties).to.have.property('id');
          done();
      }).catch((err) => {
        console.log(err);
        done(err);
      });
    });
  });

  describe('#validate', () => {
    it('Should validate the obj for the given schema', (done) => {
      let manager = new SchemaManager();

      
      manager.getSchema(pathMockSchema).then((schema) => {
        let data = {
          id: "aaa-aaaa-aaaa-a-a-aaaa",
          name: "user name",
          lastName: "user last name",
          alias: "user alias",
          email: "user@user.com",
          pwd: "asASFWFAS$%&#@AASd",
          appId: "bbb-bbb-bb-b-bbb-b-b-bbbbb",
          role: ["admin"]
        }
         
        manager.validate(data, schema).then((result) => {
          expect(result.valid).to.equals(false);
          done();
        });
      });
    });

    it('Should validate the wrong email', (done) => {
      let manager = new SchemaManager();

      
      manager.getSchema(pathMockSchema).then((schema) => {
        let data = {
          "id": "aaa-aaaa-aaaa-a-a-aaaa",
          "name": "user name",
          "lastName": "user last name",
          "alias": "user alias",
          "email": "user.com",
          "pwd": "asASFWFAS$%&#@AASd",
          "appId": "bbb-bbb-bb-b-bbb-b-b-bbbbb",
          "role": ["admin"]
        }
         
        manager.validate(data, schema).then((result) => {
          expect(result.valid).to.equals(false);
          expect(result.errors).to.have.lengthOf(2);
          done();
        });
      });
    });

    it('Should validate the wrong email', (done) => {
      let manager = new SchemaManager();

      
      manager.getSchema(pathMockSchema).then((schema) => {
        let data = {
          "id": "aaa-aaaa-aaaa-a-a-aaaa",
          "name": "user name",
          "lastName": "user last name",
          "alias": "user alias",
          "email": "user.com",
          "pwd": "asASFWFAS$%&#@AASd",
          "appId": "bbb-bbb-bb-b-bbb-b-b-bbbbb",
          "role": ["admin"]
        }
         
        manager.validate(data, schema)
        .then((result) => {
          expect(result.valid).to.equals(false);
          expect(result.errors).to.have.lengthOf(1);
          done();
        })
        .catch((err) => {
          console.log(err);
          done();
        });
      });
    });
  });
});