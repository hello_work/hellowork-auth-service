'use strict';

var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./../src/config');

let payload = {
            sub: "",
            produced: moment(),
            expired: "2147483647",
            app: "0000-0000-0000-0000",
            admin: false,
            roles: ["developer"]
        }

console.log('Payload:');
console.log(payload);

var tokenSign = config.TOKEN_SECRET;

if (!tokenSign) {
    return null;
}

console.log('Token:');
console.log(jwt.encode(payload, tokenSign));