/**
 * Scripts to configure the database
 */

//User collection.
var config = require('./../src/config.js');
var MongoClient = require('mongodb').MongoClient;
var userCollectionName = config.MONGO_USER_COLLECTION;
var url = config.MONGO_CONNECTION;

MongoClient.connect(url, (err, db) => {
    if (db) {
        db.collection(userCollectionName).createIndex( { "email": 1 }, { unique: true } )
        .then((res) => {
            console.info("User collection index: Created Success");
        })
        .catch((err) => {
            console.error("User collection index: ERROR: " + err);
        });
        db.close();
    } else {
        return reject(err);
    }
});