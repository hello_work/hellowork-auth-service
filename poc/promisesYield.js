"use strict";

var promesa1 = () => {
    return new Promise((resolve, reject) => { resolve("promesa1"); console.log("promesa1") });
}

var promesa2 = () => {
    return new Promise((resolve, reject) => { resolve("promesa2"); console.log("promesa2") });
}

var promesa3 = () => {
    return new Promise((resolve, reject) => { resolve("promesa3"); console.log("promesa3") });
}

var co = require('co');

co(function*() {
    "use_strict";
    let p1 = yield promesa1();
    console.log("test-promesas1");
    console.log(p1);

    let p2 = yield promesa2();
    console.log("test-promesas2");
    console.log(p2);

    let p3 = yield promesa3();
    console.log("test-promesas3");
    console.log(p3);
})();