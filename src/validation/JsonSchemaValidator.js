"use strict";

let Ajv = require('ajv');

class JsonSchemaValidator {

    validate (data, schema) {
        return new Promise((resolve, reject) => {
            let ajv = new Ajv({'allErrors': true});
            let valid = ajv.validate(schema, data);
            
            if (!valid) {
                return resolve({valid:false, errors: ajv.errors});
            }
            
            return resolve({valid:true, errors: []});
        });
    }
}

module.exports = JsonSchemaValidator;