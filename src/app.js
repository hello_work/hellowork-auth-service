"use strict";

var express = require ('express');
var bodyParser = require('body-parser');
var AuthController = require('./auth/AuthController');
var UserController = require('./user/UserController');    
var AuthenticationUtils = require('./auth/AuthenticationUtils');
var config = require('./config');
var app = express();

app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({extended: true}));  

app.use('/v1/users/*', AuthenticationUtils.checkAuthHeader);

app.post('/v1/auth/signUp/schema', new AuthController().getSignSchema)
app.post('/v1/auth/signUp', new AuthController().logIn)
app.post('/v1/auth/refresh', new AuthController().refreshToken);
app.post('/v1/auth/validate', new AuthController().validateToken);
app.post('/v1/login', new AuthController().logIn);

app.get('/v1/users/schema',  new UserController().getUserSchema);
app.get('/v1/users/:id', new UserController().getUserById);
app.post('/v1/users', new UserController().createUser);


app.all('/*', (req, res, next) => {
  return res.status(404).send({message: "Resource not found"});
});

//Handling unexpected errors
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Internal error!');
});

app.listen(config.PORT, () => {  
  console.log("Node server running on http://localhost:" + config.PORT);
});