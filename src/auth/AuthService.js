"use strict";

let config = require('./../config');
let UserService = require('./../user/UserService');
let moment = require('moment');
let jwt = require('jwt-simple');

class AuthService {
    
    logIn (signUp) {
        return new Promise((resolve, reject) => {
            let userSrv = new UserService();
            let userInfo = signUp.email;
            let password = signUp.pwd;

            userSrv.getUserByEmail(userInfo)
            .then((user) => {
                if (user && user.pwd == password) {
                    let token = this.createToken(user._id, user.appId, user.roles);
                    return resolve({logIn: true, token: token});
                }
                
                return resolve({logIn: false, token: null})
            })
            .catch((err) => {
                var errResponse = {
                        logIn: false, 
                        token: null, 
                        error: true, 
                        errorType: "Internal", 
                        message: "Error getting",
                        internalError: err
                    };
                console.log(errResponse);
                reject(errResponse);
            });
        });
    }
        
    refreshToken (token) {
        var valid = this.validateToken(token);

        if (valid) {
            let payload = jwt.decode(token, config.TOKEN_SECRET);
            return this.createToken(payload.sub, payload.app, payload.roles);
        }

        return null;
    }

    validateToken (token) {
        try {
            let payload = jwt.decode(token, config.TOKEN_SECRET);

            if(payload.expired < moment().unix()) {
                return false;
            }
        } catch (err) {
            console.error(err);
            return false;
        }

        return true;
    }

    createToken (userId, appId, roles) {
        let now = moment().unix();
        let exp = moment().add(config.TOKEN_EXPIRATION_SECS, 'seconds').unix();
        let tokenSign = config.TOKEN_SECRET;
        
        if (!tokenSign) {
            return null;
        }
        
        let payload = {
            sub: userId,
            produced: now,
            expired: exp,
            app: appId,
            admin: roles.indexOf('admin') > -1,
            roles: roles
        }
        
        return jwt.encode(payload, tokenSign);
    }
}

module.exports = AuthService;