"use strict";

let AuthService = require('./AuthService');

class AuthenticationUtils {

    static checkAuthHeader (req, res, next) {  
    
    if(!req.headers.authorization) {
        return res
            .status(400)
            .send({message: "No authentication header"});
    }

    let token = req.headers.authorization
    let service = new AuthService();

    if (service.validateToken(token)) {
        next();
        }
    else {
        return res
            .status(401)
            .send({message: "UnAuthorized token"});
        }
    }
}

module.exports = AuthenticationUtils;