"use strict";

let AuthService = require('./AuthService');
let SchemaManager = require('./../validation/JsonSchemaValidator');
let path = require('path');
const signUpSchema = require('./schemas/signUp');
const tokenRequestSchema = require('./schemas/tokenRequest');


class AuthController {

    logIn (req, res) {
        let signUpData = req.body;
        let schManager = new SchemaManager();
        
        schManager.validate(signUpData, signUpSchema)
        .then((validity) => {
            if (validity.valid) {
                let authService = new AuthService();
                authService.logIn(signUpData)
                .then((logInResponse) => {
                    if (logInResponse.logIn) {
                        res.status(200).send(
                            {
                                logIn: true,
                                token: logInResponse.token, 
                                messageInfo: "Log In success!",
                                error: false
                            }
                        );
                    } else {
                        res.status(200).send(
                            {
                                logIn: false,
                                token: null, 
                                messageInfo: "User or password wrong",
                                error: false
                            }
                        );
                    }
                })
                .catch((logInErr) => {
                    res.status(500).send(
                    {
                        logIn: false,
                        token: null, 
                        messageInfo: "Error during sign up process",
                        error: true, 
                        errorType: "Internal"
                    });
                    
                 });
            } else {
                console.log({
                        logIn: false, 
                        token: null, 
                        error: true, 
                        errorType: "Validation", 
                        message: "Validation error, signUp data is wrong",
                        fields: validity.errors,
                        dataRequest: signUpData
                    });

                res.status(400).send(
                    {
                        logIn: false,
                        token: null, 
                        messageInfo: "Validation error",
                        error: true, 
                        errorType: "ValidationError",
                        fields: validity.errors,
                    });
            }
        });
    }
    
    refreshToken (req, res) {
        let data = req.body;
        let mngr = new SchemaManager();
        mngr.validate(data, tokenRequestSchema)
        .then((validity) => {
            if (validity.valid) {
                let serv = new AuthService();
                let newToken = serv.refreshToken(data.token);

                if (newToken) {
                    return res.send({
                        token: newToken,
                        messageInfo: "Refresh token success"
                    })
                } else {
                    return res.status(401).send({
                        token: null,
                        messageInfo: "Token not valid",
                        error: true,
                        errorType: "TokenNotValidError"
                    });
                }
            } else {
                console.log({ 
                        errorType: "Validation", 
                        message: "Validation error, tokenRequest data is wrong",
                        fields: validity.errors,
                        dataRequest: data
                    });

                return res.status(400).send(
                    {
                        token: null, 
                        messageInfo: "Validation error",
                        error: true, 
                        errorType: "ValidationError",
                        fields: validity.errors,
                    });
            }
        })
        .catch();
    }

    validateToken (req, res) {
        let data = req.body;
        let mngr = new SchemaManager();
        mngr.validate(data, tokenRequestSchema)
        .then((validity) => {
            if (validity.valid) {
                let serv = new AuthService();
                let valid = serv.validateToken(data.token);

                if (valid) {
                    return res.send({
                        valid: true,
                        messageInfo: "Valid token"
                    });
                } else {
                    return res.status(401).send({
                        valid: false,
                        messageInfo: "Token not valid",
                        error: true,
                        errorType: "TokenNotValidError"
                    });
                }
            } else {
                console.log({
                        errorType: "ValidationError", 
                        message: "Validation error, tokenRequest data is wrong",
                        fields: validity.errors,
                        dataRequest: data
                    });

                return res.status(400).send(
                    {
                        token: null, 
                        messageInfo: "Validation error",
                        error: true, 
                        errorType: "ValidationError",
                        fields: validity.errors,
                    });
            }
        })
        .catch();
    }

    getSignSchema (req, res) {
        return res.send(signUpSchema);        
    }

    getTokenRequestSchema (req, res) {
        return res.send(tokenRequestSchema);        
    }
}

module.exports = AuthController;