"use strict";
module.exports = {
  PORT: process.env.APP_PORT || 10001,
  TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecreto",
  TOKEN_EXPIRATION_SECS: process.env.TOKEN_EXPIRATION_SECS || 5 * 60,
  MONGO_CONNECTION: process.env.MONGO_CONNECTION ||"mongodb://localhost:27017/test",
  MONGO_USER_COLLECTION: process.env.MONGO_USER_COLLECTION || "auth.users"
};