"use strict";

let UserDao = require('./UserDao');

class UserService { 
    
    createUser (user) {
        return new Promise((resolve, reject) => {
            
            let dao = new UserDao();
            
            dao.createUser(user)
                .then((opResult) => {
                    return resolve(opResult.insertedId.toString());
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
    
    getUserByEmail (userInfo) {
        return new Promise((resolve, reject) => {
            
            let dao = new UserDao();
            dao.getUserByEmail(userInfo)
            .then((user) => {
                return resolve(user);
            })
            .catch((err) => {
                return reject(err);
            });
        });
    }
    
    getUserById (id) {
        return new Promise((resolve, reject) => {
            
            let dao = new UserDao();
            dao.getUserById(id)
            .then(function (user) {
                return resolve(user);
            })
            .catch(function (err) {
                return reject(err);
            });
        });
    }
}

module.exports = UserService;