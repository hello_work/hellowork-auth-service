class DuplicateKeyError extends Error {
    
    constructor (message, inner) {
        super(message);
        this.message = message;
        this.inner = inner;
        this.key = null;
        this.value = null;
    }    
}

module.exports = DuplicateKeyError