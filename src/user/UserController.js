"use strict";

let UserService = require('./UserService');
let DuplicateKeyError = require('./DuplicateKeyError');
let SchemaManager = require('./../validation/JsonSchemaValidator');
let path = require('path')
const userSchema = require('./schemas/user.json');

class UserController {
    
    getUserById (req, res) {

        let srv = new  UserService();
        let id = parseInt(req.params.id);
        
        if (id) {
            srv.getUserById(id).then((data) => {
                return res.send(data);
            }).catch((err) => {
                console.log(err);
                return res.status(500).send({message: "Error getting user"});
            });
        } else {
            return res.status(400).send("id is requires -> use /users/[userId]")
        }   
    }

    createUser (req, res) {
        let srv = new UserService();
        let user = req.body;
        let schemaManager = new SchemaManager();
        
        schemaManager.validate(user, userSchema)
        .then((resp) => {
            if (resp.valid) {
                srv.createUser(user).then((userId) => 
                    res.send({ error: false, id: userId, message: 'User created correctly' })
                ).catch((err) => {

                    if (err instanceof DuplicateKeyError)  {
                        console.log(err);
                        return res.status(409).send(
                            {
                                error: true, 
                                id: null, 
                                message: err.value + " is already used", 
                                errorType: err.name, 
                                key: err.key, 
                                value: err.value 
                            });    
                    }

                    console.error(err);
                    return res.status(500).send({error: true, id: null, message: "Error creating user" });
                })
            } else {
                res.status(400).send({ message: 'Validation error', fields: resp.errors });
            }
        })
        .catch((err) => {
            console.error("UserService -> createUser -> schemaValidationError.catch -> msg:" + err);
            res.status(500).send({ error: true, message:"Error creating user" });
        });
    }

    getUserSchema (req, res) {
        let mngr = new SchemaManager();
        let filePath = path.join(__dirname, 'user.json'); 
        mngr.getSchema(filePath).then((schema) => {
            return res.send(schema);
        }).catch((err) => {
            console.error("UserService -> getUserSchema -> getSchema.catch -> msg:" + err);
            return res.status(500).send({message: 'User schema not available'});
        });
    }
}

module.exports = UserController