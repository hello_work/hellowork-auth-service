"use strict";

let DuplicateKeyError = require('./DuplicateKeyError');

const config = require('./../config');
const MongoClient = require('mongodb').MongoClient;
const uuidV4 = require('uuid/v4');
const userCollectionName = config.MONGO_USER_COLLECTION;
const url  = config.MONGO_CONNECTION;

class UserDao {
    
    createUser (user) {
        return new Promise((resolve, reject) => {
            let connectPromise = MongoClient.connect(url)

            connectPromise.then(db => {
                let options = { w: 'majority', wtimeout: 2000, serializeFunctions: true };
                
                if (!user.id) {
                    user._id = uuidV4();
                    user.id = user._id;
                }
                else {
                    user._id = user.id;
                }
                
                let insertPromise = db.collection(userCollectionName).insertOne(user, options)
                insertPromise.then(r => {
                    db.close();
                    return resolve(r);
                }).catch(err => { 
                    db.close();
                    if (err.code == 11000) {
                        var error = new DuplicateKeyError("Duplicate index", err)
                        error.value = user.email;
                        error.key = '.email';
                        return reject(error);
                    }

                    return reject(err);
                });
            }).catch(err => {
                return reject(err);
            });
        });
    }

    deleteUser (user) {
        return new Promise((resolve, reject) => {
            throw "not implemented"
        });
    }

    getUserByEmail (infoUser) {
        return new Promise((resolve, reject) => {

            MongoClient.connect(url, (err, db) => {
                if (db) {
                    let col = db.collection(userCollectionName);
                    col.findOne({ email:infoUser})
                        .then((user) => {
                            return resolve(user);  
                        }).catch((err) => {
                            return reject(err);
                        });
                } else {
                    return reject(err);
                }

                db.close();
            });
        });
    }

    getUserById (userId) {
        return new Promise((resolve, reject) => {

            MongoClient.connect(url, (err, db) => {
                if (db) {
                    let col = db.collection(userCollectionName);
                    col.findOne({'_id':userId}).then((data) => {
                        return resolve(data);
                    }).catch((err) => {
                        return reject(err);
                    });
                    db.close();
                } else {
                    return reject(err);
                }
            });
        });
    }
    
    updateUser (user) {
        return new Promise((resolve, reject) => {
            throw "not implemented"
        });
    }
}

module.exports = UserDao;